package com.ontime.taxi.rider.events;

import com.ontime.taxi.common.events.BaseResultEvent;
import com.ontime.taxi.common.utils.ServerResponse;

public class AddCouponResultEvent extends BaseResultEvent {
    public AddCouponResultEvent(){
        super(ServerResponse.REQUEST_TIMEOUT);
    }
    public AddCouponResultEvent(Object... args) {
        super(args);
    }
}
