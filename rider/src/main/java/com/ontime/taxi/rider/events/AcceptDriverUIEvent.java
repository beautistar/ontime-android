package com.ontime.taxi.rider.events;

import com.ontime.taxi.common.models.DriverInfo;

public class AcceptDriverUIEvent {
    public DriverInfo driverInfo;
    public AcceptDriverUIEvent(DriverInfo driverInfo){
        this.driverInfo = driverInfo;
    }
}
