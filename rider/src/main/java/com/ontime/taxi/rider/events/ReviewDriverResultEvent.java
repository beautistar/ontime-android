package com.ontime.taxi.rider.events;

import com.ontime.taxi.common.events.BaseResultEvent;

public class ReviewDriverResultEvent extends BaseResultEvent {
    public ReviewDriverResultEvent(int response) {
        super(response);
    }
}
