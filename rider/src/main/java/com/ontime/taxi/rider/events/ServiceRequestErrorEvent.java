package com.ontime.taxi.rider.events;

import com.ontime.taxi.common.events.BaseResultEvent;

public class ServiceRequestErrorEvent extends BaseResultEvent {
    public ServiceRequestErrorEvent(int response,String message) {
        super(response,message);
    }
    public ServiceRequestErrorEvent(int response) {
        super(response);
    }
}
