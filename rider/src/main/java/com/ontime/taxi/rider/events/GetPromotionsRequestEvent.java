package com.ontime.taxi.rider.events;

import com.ontime.taxi.common.events.BaseRequestEvent;

public class GetPromotionsRequestEvent extends BaseRequestEvent {
    public GetPromotionsRequestEvent() {
        super(new GetPromotionsResultEvent());
    }
}
