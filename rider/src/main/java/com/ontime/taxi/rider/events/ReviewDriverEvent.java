package com.ontime.taxi.rider.events;

import com.ontime.taxi.common.events.BaseRequestEvent;
import com.ontime.taxi.common.models.Review;
import com.ontime.taxi.common.utils.ServerResponse;

public class ReviewDriverEvent extends BaseRequestEvent {
    public Review review;
    public ReviewDriverEvent(Review review){
        super(new ReviewDriverResultEvent(ServerResponse.REQUEST_TIMEOUT.getValue()));
        this.review = review;
    }
}
