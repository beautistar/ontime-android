package com.ontime.taxi.rider.events;

import com.ontime.taxi.common.events.BaseRequestEvent;

public class GetCouponsRequestEvent extends BaseRequestEvent {
    public GetCouponsRequestEvent() {
        super(new GetCouponsResultEvent());
    }
}
