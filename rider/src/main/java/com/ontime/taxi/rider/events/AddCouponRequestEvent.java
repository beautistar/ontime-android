package com.ontime.taxi.rider.events;

import com.ontime.taxi.common.events.BaseRequestEvent;

public class AddCouponRequestEvent extends BaseRequestEvent {
    public String code;
    public AddCouponRequestEvent(String code) {
        super(new AddCouponResultEvent());
        this.code = code;
    }
}
