package com.ontime.taxi.common.models;


public enum Gender {
    unknown,
    male,
    female
}
