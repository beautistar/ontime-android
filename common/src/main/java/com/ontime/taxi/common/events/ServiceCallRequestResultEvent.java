package com.ontime.taxi.common.events;

public class ServiceCallRequestResultEvent extends BaseResultEvent {
    public ServiceCallRequestResultEvent(int code) {
        super(code);
    }
}
