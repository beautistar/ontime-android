package com.ontime.taxi.common.events;

public class WriteComplaintResultEvent extends BaseResultEvent {
    public WriteComplaintResultEvent(int code) {
        super(code);
    }
}
