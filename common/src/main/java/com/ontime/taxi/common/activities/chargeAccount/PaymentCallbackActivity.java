package com.ontime.taxi.common.activities.chargeAccount;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ontime.taxi.common.R;
import com.ontime.taxi.common.components.BaseActivity;
import com.ontime.taxi.common.events.ChargeAccountEvent;

public class PaymentCallbackActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_callback);
        eventBus.post(new ChargeAccountEvent("online","token",100));
    }
}
