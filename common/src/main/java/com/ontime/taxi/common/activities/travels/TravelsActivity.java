package com.ontime.taxi.common.activities.travels;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.ListView;

import com.ontime.taxi.common.R;
import com.ontime.taxi.common.activities.travels.adapters.TravelsRecyclerViewAdapter;
import com.ontime.taxi.common.activities.travels.fragments.WriteComplaintDialog;
import com.ontime.taxi.common.components.BaseActivity;
import com.ontime.taxi.common.components.LoadingDialog;
import com.ontime.taxi.common.databinding.ActivityTravelsBinding;
import com.ontime.taxi.common.events.GetTravelsEvent;
import com.ontime.taxi.common.events.GetTravelsResultEvent;
import com.ontime.taxi.common.events.HideTravelEvent;
import com.ontime.taxi.common.events.HideTravelResultEvent;
import com.ontime.taxi.common.events.WriteComplaintEvent;
import com.ontime.taxi.common.events.WriteComplaintResultEvent;
import com.ontime.taxi.common.models.Travel;
import com.ontime.taxi.common.utils.AlertDialogBuilder;
import com.ontime.taxi.common.utils.AlerterHelper;
import com.tylersuehr.esr.ContentItemLoadingStateFactory;
import com.tylersuehr.esr.EmptyStateRecyclerView;
import com.tylersuehr.esr.ImageTextStateDisplay;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

public class TravelsActivity extends BaseActivity implements WriteComplaintDialog.onWriteComplaintInteractionListener {

    private String subjectText = "";
    private String contentText = "";
    private long lastSelectedTravelId;
    ActivityTravelsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(TravelsActivity.this, R.layout.activity_travels);
        //setContentView(R.layout.activity_travels);
        initializeToolbar(getString(R.string.title_travel));
        binding.recyclerView.setStateDisplay(EmptyStateRecyclerView.STATE_LOADING, ContentItemLoadingStateFactory.newListLoadingState(this));
        binding.recyclerView.setStateDisplay(EmptyStateRecyclerView.STATE_EMPTY, new ImageTextStateDisplay(this, R.drawable.empty_state, "Oops!", "Nothing to show here :("));
        binding.recyclerView.setStateDisplay(EmptyStateRecyclerView.STATE_ERROR, new ImageTextStateDisplay(this, R.drawable.empty_state, "SORRY...!", "Something went wrong :("));
        binding.recyclerView.invokeState(EmptyStateRecyclerView.STATE_LOADING);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getTravels();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onWriteComplaintResult(WriteComplaintResultEvent event){
        LoadingDialog.dismiss();
        if(event.hasError()){
            event.showError(TravelsActivity.this, result -> {
                if(result == AlertDialogBuilder.DialogResult.RETRY){
                    eventBus.post(new WriteComplaintEvent(lastSelectedTravelId, subjectText, contentText));
                    LoadingDialog.show(TravelsActivity.this,getString(R.string.sending_complaint));
                }
            });
        } else {
            AlerterHelper.showInfo(TravelsActivity.this,getString(R.string.message_complaint_sent));
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHideTravelResult(HideTravelResultEvent event){
        if(event.hasError())
            return;
        AlerterHelper.showInfo(TravelsActivity.this,getString(R.string.info_travel_hidden));
        getTravels();

    }
    private void getTravels() {
        eventBus.post(new GetTravelsEvent());
        //LoadingDialog.show(this, getString(R.string.message_loading_travels));
        binding.recyclerView.invokeState(EmptyStateRecyclerView.STATE_LOADING);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTravelsReceived(GetTravelsResultEvent event) {
        //LoadingDialog.dismiss();
        if (event.hasError()) {
            binding.recyclerView.invokeState(EmptyStateRecyclerView.STATE_ERROR);
            event.showError(TravelsActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    getTravels();
                else
                    finish();
            });
            return;
        }
        if (event.travels.size() == 0) {
            binding.recyclerView.invokeState(EmptyStateRecyclerView.STATE_EMPTY);
            return;
        }
        binding.recyclerView.invokeState(EmptyStateRecyclerView.STATE_OK);

        loadList(event.travels);
    }

    private void loadList(final ArrayList<Travel> travels) {
        if (travels == null)
            return;


        final TravelsRecyclerViewAdapter adapter = new TravelsRecyclerViewAdapter(TravelsActivity.this, travels, new TravelsRecyclerViewAdapter.OnTravelItemInteractionListener() {
            @Override
            public void onHideTravel(Travel travel) {
                AlertDialogBuilder.show(TravelsActivity.this, getString(R.string.question_hide_travel), AlertDialogBuilder.DialogButton.OK_CANCEL, result -> {
                    if (result == AlertDialogBuilder.DialogResult.OK)
                        eventBus.post(new HideTravelEvent(travel.getId()));
                });
            }

            @Override
            public void onWriteComplaint(Travel travel) {
                lastSelectedTravelId = travel.getId();
                FragmentManager fm = getSupportFragmentManager();
                (new WriteComplaintDialog()).show(fm, "fragment_complaint");

            }
        });
        LinearLayoutManager llm = new LinearLayoutManager(TravelsActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(llm);
        binding.recyclerView.setAdapter(adapter);

//        final TravelListViewAdapter adapter = new TravelListViewAdapter(TravelsActivity.this,travels);
//        adapter.setHideTravelClickListener(view -> {
//            AlertDialogBuilder.show(TravelsActivity.this, getString(R.string.question_hide_travel), AlertDialogBuilder.DialogButton.OK_CANCEL, result -> {
//                if(result == AlertDialogBuilder.DialogResult.OK)
//                    eventBus.post(new HideTravelEvent((int)view.getTag()));
//            });
//        });
//        adapter.setWriteComplaintClickListener(view -> {
//            lastSelectedTravelId = (int)view.getTag();
//            FragmentManager fm = getSupportFragmentManager();
//            (new WriteComplaintDialog()).show(fm, "fragment_complaint");
//        });
//        final ListView listView = findViewById(R.id.list_travels);
//        listView.setAdapter(adapter);
//        listView.setOnItemClickListener((adapterView, view, pos, l) -> {
//            // toggle clicked cell state
//            //((FoldingCell) view).toggle(false);
//            // register in adapter that state for selected cell is toggled
//            //adapter.registerToggle(pos);
//        });
    }

    @Override
    public void onSaveComplaintClicked(WriteComplaintEvent event) {
        event.travelId = lastSelectedTravelId;
        eventBus.post(event);
        LoadingDialog.show(TravelsActivity.this,getString(R.string.sending_complaint));
    }
}