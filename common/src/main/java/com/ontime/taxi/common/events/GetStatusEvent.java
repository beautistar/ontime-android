package com.ontime.taxi.common.events;

import com.ontime.taxi.common.utils.ServerResponse;

public class GetStatusEvent extends BaseRequestEvent {
    public GetStatusEvent() {
        super(new GetStatusResultEvent(ServerResponse.REQUEST_TIMEOUT));
    }
}
