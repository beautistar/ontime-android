package com.ontime.taxi.common.interfaces;

public interface LocationChangeListener {
    void OnLocationChange(String lat, String lng);
}
