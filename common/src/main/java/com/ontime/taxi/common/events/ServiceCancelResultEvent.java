package com.ontime.taxi.common.events;

public class ServiceCancelResultEvent extends BaseResultEvent {
    public ServiceCancelResultEvent(int code) {
        super(code);
    }
}
