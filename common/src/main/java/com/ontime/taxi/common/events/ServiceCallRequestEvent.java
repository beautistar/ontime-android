package com.ontime.taxi.common.events;

import com.ontime.taxi.common.utils.ServerResponse;

public class ServiceCallRequestEvent extends BaseRequestEvent {
    public ServiceCallRequestEvent() {
        super(new ServiceCallRequestResultEvent(ServerResponse.REQUEST_TIMEOUT.getValue()));
    }
}
