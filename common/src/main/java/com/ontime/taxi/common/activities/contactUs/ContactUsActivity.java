package com.ontime.taxi.common.activities.contactUs;

import android.os.Bundle;

import com.ontime.taxi.common.R;
import com.ontime.taxi.common.components.BaseActivity;

public class ContactUsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
    }
}
