package com.ontime.taxi.driver.events;

import com.ontime.taxi.common.events.BaseRequestEvent;

public class GetServiceRequestEvent extends BaseRequestEvent {

    public String driverID;

    public GetServiceRequestEvent(String driverID){
        this.driverID = driverID;
    }
}
