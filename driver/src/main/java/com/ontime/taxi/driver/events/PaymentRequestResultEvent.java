package com.ontime.taxi.driver.events;

import com.ontime.taxi.common.events.BaseResultEvent;

public class PaymentRequestResultEvent extends BaseResultEvent {
    public PaymentRequestResultEvent(int code) {
        super(code);
    }
}
