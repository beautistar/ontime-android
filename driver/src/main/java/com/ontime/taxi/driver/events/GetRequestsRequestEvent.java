package com.ontime.taxi.driver.events;

import com.ontime.taxi.common.events.BaseRequestEvent;

public class GetRequestsRequestEvent extends BaseRequestEvent {
    public GetRequestsRequestEvent() {
        super(new GetRequestsResultEvent());
    }
}
