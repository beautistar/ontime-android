package com.ontime.taxi.driver.events;

import com.google.gson.Gson;
import com.ontime.taxi.common.events.BaseResultEvent;
import com.ontime.taxi.common.models.Media;

public class ChangeHeaderImageResultEvent extends BaseResultEvent {
    public Media media;
    public ChangeHeaderImageResultEvent(int code, String media) {
        super(code);
        this.media = new Gson().fromJson(media,Media.class);
    }
}
