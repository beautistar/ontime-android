package com.ontime.taxi.driver.events;

import com.ontime.taxi.common.events.BaseRequestEvent;
import com.ontime.taxi.common.utils.ServerResponse;

public class ChangeHeaderImageEvent extends BaseRequestEvent {
    public String path;
    public ChangeHeaderImageEvent(String path){
        super(new ChangeHeaderImageResultEvent(ServerResponse.REQUEST_TIMEOUT.getValue(),null));
        this.path = path;
    }
}
