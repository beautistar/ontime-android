package com.ontime.taxi.driver.events;

import com.ontime.taxi.common.models.Travel;

public class SendTravelInfoEvent {
    public Travel travel;
    public SendTravelInfoEvent(Travel travel) {
        this.travel = travel;
    }
}
