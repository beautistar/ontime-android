package com.ontime.taxi.driver.activities.splash;

import android.Manifest;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.ontime.taxi.common.activities.login.LoginActivity;
import com.ontime.taxi.common.components.BaseActivity;
import com.ontime.taxi.common.events.BackgroundServiceStartedEvent;
import com.ontime.taxi.common.events.ConnectEvent;
import com.ontime.taxi.common.events.ConnectResultEvent;
import com.ontime.taxi.common.events.LoginEvent;
import com.ontime.taxi.common.models.Driver;
import com.ontime.taxi.common.models.Service;
import com.ontime.taxi.common.utils.AlertDialogBuilder;
import com.ontime.taxi.common.utils.AlerterHelper;
import com.ontime.taxi.common.utils.CommonUtils;
import com.ontime.taxi.common.utils.MyPreferenceManager;
import com.ontime.taxi.driver.BuildConfig;
import com.ontime.taxi.driver.DriverEventBusIndex;
import com.ontime.taxi.driver.R;
import com.ontime.taxi.driver.activities.main.MainActivity;
import com.ontime.taxi.driver.databinding.ActivitySplashBinding;
import com.ontime.taxi.driver.events.GetServiceRequestEvent;
import com.ontime.taxi.driver.events.GetServiceResultEvent;
import com.ontime.taxi.driver.events.LoginResultEvent;
import com.ontime.taxi.driver.services.DriverService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends BaseActivity {
    MyPreferenceManager SP;
    ActivitySplashBinding binding;
    int RC_SIGN_IN = 123;
    String lastPhoneNumber = "";
    private PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            try {
                if (!isMyServiceRunning(DriverService.class))
                    startService(new Intent(SplashActivity.this, DriverService.class));
            } catch (Exception c) {
                c.printStackTrace();
            }
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {

        }
    };
    private View.OnClickListener onLoginClicked = v -> {
        String resourceName = "testMode";
        int testExists = SplashActivity.this.getResources().getIdentifier(resourceName, "string", SplashActivity.this.getPackageName());
        if (testExists > 0) {
            tryLogin(getString(testExists));
            return;
        }
        if (getResources().getBoolean(R.bool.use_custom_login)) {
            startActivityForResult(new Intent(SplashActivity.this, LoginActivity.class), RC_SIGN_IN);
        } else
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(
                                    Collections.singletonList(new AuthUI.IdpConfig.PhoneBuilder().build()))
                            .setTheme(getCurrentTheme())
                            .build(),
                    RC_SIGN_IN);
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setImmersive(true);
        showConnectionDialog = false;
        try {
            EventBus.builder().addIndex(new DriverEventBusIndex()).installDefaultEventBus();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
        if (!getString(R.string.fabric_key).equals("")) {
            //Fabric.with(this, new Crashlytics());
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        binding.loginButton.setOnClickListener(onLoginClicked);
        SP = MyPreferenceManager.getInstance(getApplicationContext());
        checkPermissions();
    }

    private void checkPermissions() {
        if (!CommonUtils.isGPSEnabled(this)) {
            AlertDialogBuilder.show(this, getString(R.string.message_enable_gps), AlertDialogBuilder.DialogButton.CANCEL_RETRY, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY) {
                    checkPermissions();
                } else {
                    finishAffinity();
                }
            });
            return;
        }
        if (CommonUtils.isInternetDisabled(this)) {
            AlertDialogBuilder.show(this, getString(R.string.message_internet_connection), AlertDialogBuilder.DialogButton.CANCEL_RETRY, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY) {
                    checkPermissions();
                } else {
                    finishAffinity();
                }
            });
            return;
        }
        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(getString(R.string.message_permission_denied))
                .setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .check();
    }

    public void tryConnect() {
        try {
            String token = SP.getString("driver_token", null);
            if (token != null && !token.isEmpty()) {
                eventBus.post(new ConnectEvent(token));
                goToLoadingMode();
            } else {
                goToLoginMode();
            }
        } catch (Exception c) {
            c.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConnectedResult(ConnectResultEvent event) {
        if (event.hasError()) {
            goToLoginMode();
            event.showError(SplashActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    tryConnect();
            });
            return;
        }

        CommonUtils.driver = new Gson().fromJson(SP.getString("driver_user", "{}"), Driver.class);

        // Get driver service information
        eventBus.post(new GetServiceRequestEvent(String.valueOf(CommonUtils.driver.getId())));

        startMainActivity();
    }

    @Subscribe
    public void onServiceStarted(BackgroundServiceStartedEvent event) {
        tryConnect();
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginResultEvent(LoginResultEvent event) {
        if (event.hasError()) {
            event.showError(SplashActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    tryLogin(lastPhoneNumber);
                else
                    finish();
            });
            return;
        }
        CommonUtils.driver = event.driver;
        SP.putString("driver_user", event.driverJson);
        SP.putString("driver_token", event.jwtToken);
        tryConnect();

    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onGetServiceRequest(GetServiceRequestEvent event) {
        try {
            URL url = new URL(getString(R.string.custom_server_address) + "getDriverService");
            HttpURLConnection client = (HttpURLConnection) url.openConnection();
            client.setRequestMethod("POST");
            client.setDoOutput(true);
            client.setDoInput(true);
            DataOutputStream wr = new DataOutputStream(client.getOutputStream());

            HashMap<String, String> postDataParams = new HashMap<>();
            postDataParams.put("driver_id", event.driverID);
            StringBuilder result = new StringBuilder();

            for (Map.Entry<String, String> entry : postDataParams.entrySet()) {
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
            wr.write(result.toString().getBytes());
            BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null)
                sb.append(line);
            String res = sb.toString();

            JSONObject obj = new JSONObject(res);
            int status = obj.getInt("result_code");

            if (status == 200) {

                GetServiceResultEvent resultEvent = new GetServiceResultEvent(status, res);
                eventBus.post(resultEvent);

            }

        } catch (Exception exception) {
            eventBus.post(new GetServiceResultEvent(404, exception.getMessage()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        tryConnect();
    }
    private void startMainActivity() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
    private void tryLogin(String phone) {
        lastPhoneNumber = phone;
        goToLoadingMode();
        if (phone.substring(0, 1).equals("+"))
            phone = phone.substring(1);
        eventBus.post(new LoginEvent(Long.valueOf(phone), BuildConfig.VERSION_CODE));
    }
    private void goToLoadingMode(){
        binding.loginButton.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.VISIBLE);
    }

    private void goToLoginMode(){
        binding.loginButton.setVisibility(View.VISIBLE);
        binding.progressBar.setVisibility(View.GONE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                if (getResources().getBoolean(R.bool.use_custom_login)) {
                    tryLogin(data.getStringExtra("mobile"));
                } else {
                    IdpResponse idpResponse = IdpResponse.fromResultIntent(data);
                    String phone;
                    if (idpResponse != null) {
                        phone = idpResponse.getPhoneNumber();
                        tryLogin(phone);
                    }
                }
                return;
            }
            AlerterHelper.showError(SplashActivity.this, getString(R.string.login_failed));
            goToLoginMode();
        }
    }
}
