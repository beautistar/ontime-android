package com.ontime.taxi.driver.events;

import com.ontime.taxi.common.events.BaseResultEvent;

public class ChangeStatusResultEvent extends BaseResultEvent {
    public ChangeStatusResultEvent(int code) {
        super(code);
    }
}
