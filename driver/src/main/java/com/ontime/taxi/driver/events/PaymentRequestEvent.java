package com.ontime.taxi.driver.events;

import com.ontime.taxi.common.events.BaseRequestEvent;
import com.ontime.taxi.common.utils.ServerResponse;

public class PaymentRequestEvent extends BaseRequestEvent {
    public PaymentRequestEvent() {
        super(new PaymentRequestResultEvent(ServerResponse.REQUEST_TIMEOUT.getValue()));
    }
}
