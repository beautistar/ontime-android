package com.ontime.taxi.driver.events;

import com.ontime.taxi.common.events.BaseResultEvent;

public class GetServiceResultEvent extends BaseResultEvent {

    public String serviceJson;

    public GetServiceResultEvent(int code, String serviceJson) {
        super(code);
        this.serviceJson = serviceJson;
    }

    public GetServiceResultEvent(Object... args) {
        super(args);
    }

}
